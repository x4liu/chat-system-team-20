package edu.ucsd.cse110.client;

public interface Constants {
	public static String ACTIVEMQ_URL = "tcp://localhost:61616";
	public static String USERNAME = "max";	
	public static String PASSWORD = "pwd";	
	public static String QUEUENAME = "test"; //so far this is what the server is listening from, so client's producer aims at this
	public static String PRODUCERQ = "producerqueue"; 
	public static String CONSUMERQ = "consumerqueue"; //queue for client to listen from
}