package edu.ucsd.cse110.client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.jms.JMSException;
import javax.jms.Session;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

//This is called by ChatClientApplication
public class loginGUI implements ActionListener {
	
private JFrame controllingFrame; //needed for dialogs
private JTextField username; 
private JTextField passwordField;
private JButton login_button;
private loginGUI login = this;
private String user_name;
private String password;

public loginGUI() {
	
	controllingFrame = new JFrame();
	controllingFrame.setSize(500,100);
	
	JComponent loginPane = createLoginPanel();
	//Lay out everything.
	JPanel textPane = new JPanel();
	textPane.add(loginPane);

	controllingFrame.add(textPane);
	controllingFrame.show();
}

private JComponent createLoginPanel() {
	JPanel p = new JPanel();
	
	//labels
	JLabel enterUserName = new JLabel("username: ");
	JLabel enterPassword = new JLabel("password: ");
	
	//textfields
	username = new JTextField(10);
	passwordField = new JTextField(10);
	
	//buttons
	login_button = new JButton("login");
	
	login_button.setActionCommand("login");
	login_button.addActionListener(this);

	p.add(enterUserName);
	p.add(username);
	p.add(enterPassword);
	p.add(passwordField);
	p.add(login_button);
	
	return p;
}


public void actionPerformed(ActionEvent e) {
	String cmd = e.getActionCommand();
	
	if (cmd.equals("login")) { //Process the password.
		
		this.user_name = username.getText();
		this.password = passwordField.getText();
	
		close();
	}
	 
}


private void close(){
	controllingFrame.dispose();
}

protected String getUsername() {
	// TODO Auto-generated method stub
	if(user_name == null) return null;
	return user_name;
}

protected String getPassword() {
	// TODO Auto-generated method stub
	if(password == null) return null;
	return password;
}

}

