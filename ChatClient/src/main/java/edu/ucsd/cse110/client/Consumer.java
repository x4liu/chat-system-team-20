package edu.ucsd.cse110.client;

import javax.jms.*;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

public class Consumer{
	
	private Destination destination;
	private MessageConsumer msgConsumer;
	private Session session;
	
	
    public Consumer(Session session) throws JMSException {
     
    	this.session = session;
        destination = session.createTemporaryQueue();

        // MessageConsumer is used for receiving (consuming) messages
        msgConsumer = session.createConsumer(destination);
        
    }
    
    public Session getSession(){
    	return this.session;
    }
    
    public Destination getDestination(){
    	return this.destination;
    }
    
    public MessageConsumer getMsgConsumer(){
    	return this.msgConsumer;
    }


}
    
    
