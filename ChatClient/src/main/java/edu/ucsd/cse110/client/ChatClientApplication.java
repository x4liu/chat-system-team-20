package edu.ucsd.cse110.client;

import java.net.URISyntaxException;
import java.io.*;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

public class ChatClientApplication{

    private static loginGUI login;
    private static ClientGUI client_gui;
    
	/*
	 * This method wires the client class to the messaging platform
	 * Notice that ChatClient does not depend on ActiveMQ (the concrete 
	 * communication platform we use) but just in the standard JMS interface.
	 */
	private static ChatClient wireClient() throws JMSException, URISyntaxException {
	
		ActiveMQConnection connection = 
				ActiveMQConnection.makeConnection(
				/*Constants.USERNAME, Constants.PASSWORD,*/ Constants.ACTIVEMQ_URL);
        connection.start();
        CloseHook.registerCloseHook(connection);
        final Session session = connection.createSession(false,
                Session.AUTO_ACKNOWLEDGE);

        login = new loginGUI();
   
        while((login.getUsername() == null) || (login.getPassword() == null)){
        	donothing();
        }
  
        return new ChatClient(session, login.getUsername(), login.getPassword());
        
	}
	
	public static void donothing(){// how you fixed a bug
		System.out.print("");
	}
	
	public static void main(String[] args) throws JMSException {
		
		boolean quit = false;
		
		try {
			//ActiveMQConnection connection = ActiveMQConnection.makeConnection(Constants.ACTIVEMQ_URL);
			
			/* 
			 * We have some other function wire the ChatClient 
			 * to the communication platform
			 */
			
			ChatClient client = wireClient();
			client.login();
			
		
			client_gui = new ClientGUI(client);

	        
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			System.out.println("exiting2");
			e.printStackTrace();
		} catch (URISyntaxException e) {
			System.out.println("exiting3");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		
	}
	
}
