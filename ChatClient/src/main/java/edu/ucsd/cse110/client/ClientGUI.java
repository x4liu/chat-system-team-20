package edu.ucsd.cse110.client;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.DefaultCaret;




public class ClientGUI implements ActionListener {

	JFrame main_frame;
	JPanel main_panel;
	List<JPanel> rows;
	
	JButton leave_button = new JButton("Leave Room");
	JButton show_button = new JButton("Show Online Users");
	JButton send_button = new JButton("Send");
	JLabel room_text = new JLabel("Rooms");
	JTable usertable;
	JComboBox<String> room_list;
	JComboBox<String> onlineUser_list;
	JTextArea conversation;
	JTextField send_bar;
	JList<String> list;
	ChatClient client;
	String[] userlist;
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	public ClientGUI(ChatClient c){
		
		 client = c;
		 c.setClient_gui(this);
		 
		 main_frame = new JFrame();
		 main_frame.setSize(new Dimension(460,600));
		 main_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 
		 rows = new LinkedList<JPanel>();
		 
		 leave_button.setActionCommand("leave");
		 send_button.setActionCommand("send");
		 show_button.setActionCommand("show");
		 leave_button.addActionListener(this);
		 send_button.addActionListener(this);
		 show_button.addActionListener(this);
		 //Online user list
		 //String onlineUsers[] = c.getOnlineUsers();
		 //onlineUser_list = new JComboBox<String>(onlineUsers);
		//Create database table
		String[] columnNames = { "online users:" };
		DefaultTableModel defTableModel = new DefaultTableModel();
		defTableModel.setColumnIdentifiers(columnNames);
		
		usertable = new JTable(defTableModel){
			  @Override
			    public boolean isCellEditable(int row, int column) {
			       return false;
			    }
		};
		//usertable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		usertable.setPreferredScrollableViewportSize(new Dimension(100,335));
		
		JScrollPane scrollPanel = new JScrollPane(usertable); //needed to show headers
		JPanel tablePanel = new JPanel();
		tablePanel.setLayout(new GridBagLayout());
		GridBagConstraints c1 = new GridBagConstraints();
		//tablePanel.setPreferredSize(new Dimension(400, 350));
		c1.fill = GridBagConstraints.HORIZONTAL;
		//c1.insets = new Insets(10,10,5,10);
		c1.weightx = 0.5;
		c1.gridx = 0;
		c1.gridy = 0;
		c1.gridwidth=1;
		tablePanel.add(scrollPanel,c1);
		 
		 //contain Chat Rooms
		 room_list = new JComboBox<String>(get_rooms());
		 room_list.setSize(100,50);
		 JPanel first_row = new JPanel();
		 first_row.add(room_text);
		 first_row.add(room_list);
		 rows.add(first_row);     
		 
		 //contain online users
		 list = new JList<String>(get_online_users()); //data has type Object[]
		 
		 JPanel second_row = new JPanel();
		 second_row.add(list);
		 rows.add(second_row);     
		 
		 //button
		 JPanel sixth_row = new JPanel();
		 sixth_row.add(leave_button);
		 sixth_row.add(show_button);
		 rows.add(sixth_row);     
		 
		 //text 
		 conversation = new JTextArea("Welcome "+c.getUserName()+" !\n", 20, 30);
		 conversation.setEditable(false);
		 conversation.setLineWrap(true);
		 conversation.setWrapStyleWord(true);
		 JScrollPane scroll = new JScrollPane(conversation,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                 JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		 JScrollBar bar = scroll.getVerticalScrollBar();
		 bar.setPreferredSize(new Dimension(20, 0));
	     //scroll.setPreferredSize(200,300);
		 c1.gridx = 1;
		 c1.gridwidth=4;
		 tablePanel.add(scroll,c1);
		 
		 JPanel seventh_row = new JPanel();
		 seventh_row.add(tablePanel);
		 //seventh_row.add(scroll);
		 rows.add(seventh_row);
		 
		 //send bar
		 JPanel eighth_row = new JPanel();
		 send_bar = new JTextField(30);
		 eighth_row.add(send_bar);
		 eighth_row.add(send_button);
		 rows.add(eighth_row);
		 
		 main_panel = new JPanel();
		 
		 // the main panel uses a vertical box layout      
		 main_panel.setLayout(new BoxLayout(main_panel, BoxLayout.Y_AXIS));           
		 
		 // and we add all rows we have initialized above              
		 for(JPanel pane : rows) {         
			 main_panel.add(pane);         
		 }         
		 
		 // add the main panel to the window      
		 main_frame.add(main_panel);    
		 main_frame.show();
	}
	
	private String[] get_online_users() {
		// TODO Auto-generated method stub
		String[] result = {""};
		return result;
	}
	
	public void set_online_users(String[] s){
		userlist = s;
		
	}
	
	private String[] get_rooms() {
		// TODO Auto-generated method stub
		String[] rooms = {"Room1"};
		return rooms;
	}

	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		String cmd = e.getActionCommand();
		try{
		if(cmd.equals("leave")){
			client.logout();
			close();
		}else if(cmd.equals("send")){
			String msg = send_bar.getText();
			//if someone is selected from the online user table, send msg to that guy
			if(usertable.getSelectedRow()>=0){
				String toUser = (String) usertable.getModel()
					.getValueAt(usertable.getSelectedRow(), 0);
				client.sendTo(toUser, msg);
			}
			else{ //if online user table is not selected at all, just send
				client.send(msg);
			}
		}else if(cmd.equals("show")){
			client.showAll();
		}
		}catch(JMSException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		
	}
	
	public void close(){
		main_frame.dispose();
	    System.exit(0);
	}
	
	public void refresh(String s) throws JMSException{
	
		if(s != null){
			if(s.equals("Wrong password!")){
				JOptionPane.showMessageDialog(main_frame,
						"Invalid password. Try again.",
						"Error Message",JOptionPane.ERROR_MESSAGE);
				close();
			}
			conversation.append(s+"\n");
		}
	}
	
	protected void updateTable() {
		DefaultTableModel dtm = (DefaultTableModel) usertable.getModel();
		dtm.setRowCount(0);
		int i=0;
		while (i<client.onlineUsers.length) {
			String user = client.onlineUsers[i];
			dtm.addRow(new Object[] {user});
			i++;
		}
	}

}
