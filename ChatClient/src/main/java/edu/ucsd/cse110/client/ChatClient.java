package edu.ucsd.cse110.client;

import java.util.LinkedList;
import java.util.Observer;
import java.util.Queue;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

public class ChatClient implements parseArg, MessageListener{
	private Producer producer;
	private Consumer consumer;
	private String userName;
	private String password;
	private Session session;
	private Destination dest;
	private boolean onlineStatus = false;
	private ClientGUI client_gui;
	public String[] onlineUsers;
	
	public ChatClient(Session session, String userName, String password) throws JMSException {
		super();
		try {
		this.userName = userName;
		this.password = password;
		this.session = session;
		this.producer = new Producer(session);
		this.consumer = new Consumer(session); 
		this.dest = consumer.getDestination();
		getMsgConsumer().setMessageListener( this );
		}
		catch (JMSException e) {
            System.out.println( "Failed to initiate ChatClient.");
		}
	} 
	
	//convert Message to String
	public String convertMsg(Message msg){
		try {
			return ((TextMessage)msg).getText();
		} catch (JMSException e) {
			System.out.println("Error: Failed to convert Message to String!");
			e.printStackTrace();
		}
		return null;
	}

	//public String[] parseIntoTokens(String s){
	//	String delim = "[ ]+";
	//	return s.split(delim);
	//}
	
	protected String getUserName() {
		return userName;
	}
	
	private String getPassword(){
		return password;
	}
	
	public Session getSession(){
		return this.session;
	}
	
	public TextMessage createMessage(String type, String str) throws JMSException{
		TextMessage newMessage = producer.getSession().createTextMessage();
		newMessage.setJMSType(type);
		newMessage.setJMSReplyTo(dest);
		newMessage.setText(str);
		
		return newMessage;
	
	}
    
	public void send(String msg) throws JMSException {
		//System.out.println("Sending message...");
		TextMessage mesg = createMessage("regular","["+ getUserName()+ "]: "+ msg);
		producer.getMsgProducer().send(mesg);
		//System.out.print("message sent !\n");
	}
	
	public void sendTo(String toUser, String msg) throws JMSException{
		TextMessage mesg = createMessage("regular","["+ getUserName()+ "]: /"+toUser+" "+msg);
		producer.getMsgProducer().send(mesg);
	}
	
	
	public MessageProducer getMsgProducer(){
		return this.producer.getMsgProducer();
	}
	
	public MessageConsumer getMsgConsumer(){
		return this.consumer.getMsgConsumer();
	}
	
	public boolean verify(String pass) {
		return this.password.equals(pass);
	}
	
	public boolean isOnline() {
		return onlineStatus;
	}
	
	public void showAll() throws JMSException{
		//TextMessage mesg = createMessage("show",getUserName());
		//producer.getMsgProducer().send(mesg);
	
		String names = "[System]: List of online users: ";
		for(int i=0;i<onlineUsers.length;i++){
			//System.out.println(onlineUsers[i]+"\n"); //testing
			names = names + onlineUsers[i]+", ";
		}
		if(names.length()>2){
			names=names.substring(0, names.length()-2); //trim off the last ", "
		}
		//System.out.println(names);
		client_gui.refresh(names);
		client_gui.updateTable();
	}
		
	public void login() throws JMSException{
		//this.onlineStatus = true;
		TextMessage mesg = createMessage("login",getUserName()+":"+getPassword());
		producer.getMsgProducer().send(mesg);
		
		//System.out.println(this.getUserName() + " wired.");
	}
	
	public void logout() throws JMSException{
		TextMessage msg = createMessage("logout", getUserName());
		producer.getMsgProducer().send(msg);
		//this.onlineStatus = false;
		//System.out.println(this.getUserName() + " logged off.");
		}

	public void setOnline(){
		this.onlineStatus = true;
	}
	
	public void setOffline(){
		this.onlineStatus = false;
	}
	
	public void updateOnlineUsers(String s){
		onlineUsers = null;
		s=s.substring(s.indexOf('=')+1, s.length()-1); //trim off front
     	//System.out.println(s);  testing
      	onlineUsers = s.split(":"); //store online usernames into onlineUsers array
      	//System.out.println("trimmed username:\n"+onlineUsers); testing
      	client_gui.updateTable();
	}
	////////////////////////////////////////////////
	//////////////RECEIVE MESSAGES/////////////////
	//////////////////////////////////////////////
	public void onMessage(Message msg) {
		if(msg instanceof TextMessage){
            String s = convertMsg(msg);   
            //if this is a message to update onlineUsers
            if(s.startsWith("@")&&s.indexOf('=')==13){
             	updateOnlineUsers(s);
            }
            else{
             System.out.println(s);
             try {
					client_gui.refresh(s);
				} catch (JMSException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                if(s.equals("You are now logged in.")){
                	this.setOnline();
                }
                if(s.equals("You are now logged out.")){
                	this.setOffline();
                }
            }
		}
	}

	public String[] parseIntoTokens(String s) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public void setClient_gui(ClientGUI s){
		this.client_gui = s;
	}

	public String[] getOnlineUsers(){
		return this.onlineUsers;
	}
	
}
	
