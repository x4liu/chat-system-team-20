package edu.ucsd.cse110.client;

import javax.jms.*;

import org.apache.activemq.ActiveMQConnectionFactory;

public class Producer {

	private Session session;
	private Destination destination;
	private MessageProducer msgProducer;
	
    public Producer (Session session) throws JMSException {
    	
    	this.session = session;
    	 destination = session.createQueue(Constants.QUEUENAME);
    	
        // MessageProducer is used for sending messages (as opposed
        // to MessageConsumer which is used for receiving them)
        msgProducer = session.createProducer(destination);
        
    }
    
    public Session getSession(){
    	return this.session;
    }
   

    public MessageProducer getMsgProducer(){
    	return this.msgProducer;
    }
    
    
    
    
    }
