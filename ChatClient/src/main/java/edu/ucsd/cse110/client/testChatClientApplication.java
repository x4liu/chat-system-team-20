package edu.ucsd.cse110.client;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.IOException;

import org.apache.activemq.ActiveMQConnection;
import org.junit.Test;

import java.net.URISyntaxException;

import javax.jms.JMSException;


public class testChatClientApplication {

	//////////////////////////////////////////////
	/* 
	 * if this way of testing doesnt work.. ignore it
	 */
	//////////////////////////////////////////////
	
	@Test
	/* test ChatClientApplication */
	public void testMain() throws IOException, JMSException {
		 
        //Setup output.
        final ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));
       
        //call main
        ChatClientApplication.main(null);
       
        //simulate user input
        ByteArrayInputStream in = new ByteArrayInputStream("user".getBytes());
        System.setIn(in);

        //verifying output
        final String nl ="\r\n";
        assertEquals("Enter User Name: user"+nl
                        +"user wired."+ nl + "Say Hello...", output.toString());
       
        in.read("quit".getBytes(), 0, 255);
        System.setIn(in);

}

@Test  
public void testQuit() throws JMSException {
        //Setup output.
        final ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));
       
        //call main
        ChatClientApplication.main(null);
       
        //simulate user input
        ByteArrayInputStream in = new ByteArrayInputStream("user".getBytes());
        System.setIn(in);
       
        in.read("quit".getBytes(), 0, 255);
        System.setIn(in);
       
        assertEquals("Closing ActiveMQ connection", output.toString());
       
        //ADD A TEST FOR COUNTDOWN DEPENDING ON WHAT OUTPUT IS
       
}

@Test
public void testPassword() throws JMSException {
        //Setup output.
        final ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));
       
        //call main
        ChatClientApplication.main(null);
       
        //simulate user input
        ByteArrayInputStream in = new ByteArrayInputStream("user".getBytes());
        System.setIn(in);
       
        //change the string depending on how the implemented string is
        assertEquals("Enter password:", output.toString());
       
        in.read("WRONGPW".getBytes(), 0, 255);
        System.setIn(in);
       
        assertEquals("Closing ActiveMQ connection", output.toString());
}

@Test
public void testWho() throws JMSException {
        //Setup output.
        final ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));
       
        //call main
        ChatClientApplication.main(null);
       
        //simulate user input
        ByteArrayInputStream in = new ByteArrayInputStream("user".getBytes());
        System.setIn(in);
       
        assertEquals("Enter password:", output.toString());
       
        in.read(Constants.PASSWORD.getBytes(), 0, 255);
        System.setIn(in);
       
        //CHANGE BASED ON IMPLEMENTATION
        in.read("/who".getBytes(), 0, 255);
        System.setIn(in);
       
        assertEquals("Users online: user", output.toString());
}

}
