package edu.ucsd.cse110.client;

import static org.junit.Assert.*;

import java.net.URISyntaxException;

import javax.jms.JMSException;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnection;
import org.junit.Test;

public class testChatClient {

	/* 
	 * test ChatClient Constructor
	 * by manually creating needed producer and session
	 */
	@Test
	public void testChatClientConstr() {
		String userName = "Tester";
		String password = "testPassword";
		try {
			ActiveMQConnection connection = ActiveMQConnection.makeConnection(Constants.ACTIVEMQ_URL);
			connection.start();
			CloseHook.registerCloseHook(connection);
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        
      //creating client
      		ChatClient client = new ChatClient(session, userName, password);
      //Client should be offline initially
      		assertFalse(client.isOnline());
      //Check userName
      		assertEquals("Tester", client.getUserName());
      //Check password
            assertTrue(client.verify("testPassword"));
            connection.close();
		} catch (JMSException e) {
			System.out.println( "Failed to initiate ChatClient.");
			fail();
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail();
		}
	}
	@Test
	public void testgetUserName() {
		String userName = "Student";
		String password = "testPassword";
		try {
			ActiveMQConnection connection = ActiveMQConnection.makeConnection(Constants.ACTIVEMQ_URL);
			connection.start();
			CloseHook.registerCloseHook(connection);
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        
      //creating client
      		ChatClient client = new ChatClient(session, userName, password);
      //CHECKING userName//
      		assertEquals("Student", client.getUserName());
      //CHECKING userName//
      		assertFalse(client.getUserName().equals("Tester"));
      		connection.close();
		} catch (JMSException e) {
			System.out.println( "Failed to initiate ChatClient.");
			fail();
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail();
		}
	}
	@Test
	public void testSend() {
		String userName = "Tester1";
		String password = "testPassword";
		try {
			ActiveMQConnection connection = ActiveMQConnection.makeConnection(Constants.ACTIVEMQ_URL);
			connection.start();
			CloseHook.registerCloseHook(connection);
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        
      //creating client
      		ChatClient client = new ChatClient(session, userName, password);
      		try {
      //SENDING message
      			client.send("Testing send()");
      		} catch (JMSException e) {
      			System.out.println("Failed to send message");
      			fail();
      		}
      		connection.close();
		} catch (JMSException e) {
			System.out.println( "Failed to initiate ChatClient.");
			fail();
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail();
		}
	}
	@Test
	public void testVerify() {
		String userName = "Tester";
		String password = "testPassword";
		try {
			ActiveMQConnection connection = ActiveMQConnection.makeConnection(Constants.ACTIVEMQ_URL);
			connection.start();
			CloseHook.registerCloseHook(connection);
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE); 
        
      //creating client
      		ChatClient client = new ChatClient(session, userName, password);
      //VERIFYING password
            assertTrue(client.verify("testPassword"));
      //VERIFYING password
            assertFalse(client.verify("Incorrect"));
            connection.close();
		} catch (JMSException e) {
			System.out.println( "Failed to initiate ChatClient.");
			fail();
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail();
		}
	}
	@Test
	public void testIsOnline() {
		String userName = "Tester3";
		String password = "testPassword";
		try {
			ActiveMQConnection connection = ActiveMQConnection.makeConnection(Constants.ACTIVEMQ_URL);
			connection.start();
			CloseHook.registerCloseHook(connection);
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        
      //creating client
      		ChatClient client = new ChatClient(session, userName, password);
      //Client should be offline initially
      		assertFalse(client.isOnline());
      //Client logging in
      		client.login();
      //Client now online
      		assertTrue(client.isOnline());
      		connection.close();
		} catch (JMSException e) {
			System.out.println( "Failed to initiate ChatClient.");
			fail();
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail();
		}
	}
	@Test
	public void testLogin() {
		String userName = "Tester4";
		String password = "testPassword";
		try {
			ActiveMQConnection connection = ActiveMQConnection.makeConnection(Constants.ACTIVEMQ_URL);
			connection.start();
			CloseHook.registerCloseHook(connection);
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        
      //creating client
      		ChatClient client = new ChatClient(session, userName, password);
      //Client offline initially, now logging in
      		client.login();
      //Client now online
      		assertTrue(client.isOnline());
      		connection.close();
		} catch (JMSException e) {
			System.out.println( "Failed to initiate ChatClient.");
			fail();
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail();
		}
	}
	@Test
	public void testLogout() {
		String userName = "Tester5";
		String password = "testPassword";
		try {
			ActiveMQConnection connection = ActiveMQConnection.makeConnection(Constants.ACTIVEMQ_URL);
			connection.start();
			CloseHook.registerCloseHook(connection);
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        
      //creating client
      		ChatClient client = new ChatClient(session, userName, password);
      //Client offline initially, now logging in
      		client.login();
      //Client now online
      		assertTrue(client.isOnline());
      //Client logging off
      		client.logout();
      //Client should now be offline
      		assertFalse(client.isOnline());
      		connection.close();
		} catch (JMSException e) {
			System.out.println( "Failed to initiate ChatClient.");
			fail();
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail();
		}
	}
	@Test
	public void testOnMessage() {
		
	}
}
