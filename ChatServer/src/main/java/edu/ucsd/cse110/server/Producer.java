package edu.ucsd.cse110.server;

import javax.jms.*;

import org.apache.activemq.ActiveMQConnectionFactory;

public class Producer {

	private String url;
	private Connection connection;
	private Session session;
	private Destination destination;
	private MessageProducer msgProducer;
	
    public Producer (String url, String queue) throws JMSException {
    	
    	this.url = url;
        // Getting JMS connection from the server and starting it
        ConnectionFactory connectionFactory =
            new ActiveMQConnectionFactory(url);
        connection = connectionFactory.createConnection();
        connection.start();

        // JMS messages are sent and received using a Session. We will
        // create here a non-transactional session object. If you want
        // to use transactions you should set the first parameter to 'true'
        session = connection.createSession(false,
            Session.AUTO_ACKNOWLEDGE);

        // Destination represents here our queue 'TESTQUEUE' on the
        // JMS server. You don't have to do anything special on the
        // server to create it, it will be created automatically.
        destination = session.createQueue(queue);

        // MessageProducer is used for sending messages (as opposed
        // to MessageConsumer which is used for receiving them)
        msgProducer = session.createProducer(destination);
 
        
        
    }
    
    public Session getSession(){
    	return this.session;
    }
    public String getUrl(){
    	return this.url;
    	}
    public MessageProducer getMsgProducer(){
    	return this.msgProducer;
    }
    
    
    
    
    }
