package edu.ucsd.cse110.server;

public interface Constants {
	public static String ACTIVEMQ_URL = "tcp://localhost:61616";
	public static String USERNAME = "max";	
	public static String PASSWORD = "pwd";	
	public static String QUEUENAME = "test";
	public static String PRODUCERQ = "producerqueue";
	public static String CONSUMERQ = "consumerqueue";
}