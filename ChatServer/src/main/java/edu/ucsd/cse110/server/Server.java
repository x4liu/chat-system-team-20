package edu.ucsd.cse110.server;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;

public class Server{

	private static JmsTemplate jms;
	private HashMap<String, String> accounts;
	private HashMap<String, Destination> onlineUser;
	public Server () {
		super();
		onlineUser = new HashMap<String, Destination>();
		accounts = new HashMap<String, String>();
		try {
			loadAccountsUser();
			System.out.println("accounts loaded.");
		} catch (IOException e) {
			System.err.println("Error: Failed to load Account.txt");
			e.printStackTrace();
		}
	}
	
	public void updateOnlineUser(){
		String usernames = "@!onlineUsers=";
		for (String onlineuser: onlineUser.keySet()){
			usernames = usernames+onlineuser+":";
		}
		
		for(Destination dest : onlineUser.values()){
			jms.convertAndSend(dest, usernames);
		}
		
		//System.out.println(usernames);
	}
	
	
	public boolean accountExist(String userName){
		return accounts.containsKey(userName);
	}
	
	public boolean authorizingAccess(String userName, String password){
		String realPass = accounts.get(userName);
		if(password != realPass){
			return false;
		}
		else{
			return true;
		}
	}
	
	//check if user is online
	public Boolean checkOnline(String username){
		return onlineUser.containsKey(username);
	}
	
	public boolean checkPassword(String username, String pwd){
		 if (this.accounts.get(username).equals(pwd)){
			 return true;
		 }
		 else{
			 return false;
		 }
	}
	
	//convert Message to String
	public String convertMsg(Message msg){
		try {
			return ((TextMessage)msg).getText();
		} catch (JMSException e) {
			System.err.println("Error: Failed to convert Message to String!");
			e.printStackTrace();
		}
		return null;
	}
	
	/* 
	 * Example use: passed in msg "[Allen]:  hi!"
	 * will return "Allen"
	 */
	public String getUserName(Message msg){
		String username = null;
		try {
			username = ((TextMessage)msg).getText();
			username = username.substring(1); //trim off the first '['
			username = username.substring(0, username.indexOf(']')); //trim off the first '['
			
		} catch (JMSException e) {
			
			e.printStackTrace();
		}
		return username;
	}
	
	public void loginUser(Message msg) throws JMSException{
		String s = convertMsg(msg);
		String username = s.substring(0, s.indexOf(':'));
		String pwd = s.substring(s.indexOf(':')+1, s.length());
		
		if(accountExist(username)){
	
			if(checkPassword(username, pwd)){

				String goodLogin = "You are now logged in.";
				reply(msg, goodLogin);
				onlineUser.put(username, msg.getJMSReplyTo());
				updateOnlineUser();
			}
			else{
				String badLogin = "Wrong password!";
				reply(msg, badLogin);
			}
		}
		else{  //if accountname doesnt exit, then register
			try {
				writeAccount(username, pwd);
			} catch (IOException e) {
				System.err.println("Error: Failed to write account");
				e.printStackTrace();
			}
			String reg = "New account registered!\n";
			accounts.put(username, pwd);
			reply(msg, reg);
			String goodLogin = "You are now logged in.\n";
			reply(msg, goodLogin);
			onlineUser.put(username, msg.getJMSReplyTo());
			updateOnlineUser();
		}
	}
	
	public void logoutUser(Message msg){
		String username = convertMsg(msg);
		if(checkOnline(username)){
			//System.out.println("should be here");
			onlineUser.remove(username);
			updateOnlineUser();
			System.out.println(username+" is now offline.\n");
			String loggedOut = "You are now logged out.\n";
			reply(msg, loggedOut);
		}
		else{
			//System.out.println("shouldn't be here");
			String notloggedOut = "You are not even logged in, how can u logged out?\n";
			reply(msg, notloggedOut);
		}
	}
	
	/////////////////////////////////////////////////////////////
	//////////////RECEIVE METHOD///////////////////////////////
	///////////////////////////////////////////////////////////
	public void receive(Message msg) throws JMSException {
		
		String s = convertMsg(msg);
		System.out.println(s); 
	
		if(msg.getJMSType().equals("login")){
			loginUser(msg);
		}
		else if(msg.getJMSType().equals("logout")){
			logoutUser(msg);
		}
		else if(msg.getJMSType().equals("regular")){
			String fromUser = getUserName(msg);
			String trimmed = trimOffUserName(s);
	
			//**if this is a message to a specific user** 
			if(trimmed.charAt(0)=='/'){
				String toUser = trimmed.substring(1, trimmed.indexOf(' '));
				//System.out.println(toUser); //testing
				String msgToSend = trimmed.substring(trimmed.indexOf(' '), trimmed.length());
				//if this user is online
				if(checkOnline(toUser)){
					jms.convertAndSend(onlineUser.get(toUser), "["+fromUser+"]: "+msgToSend);
				}
				else{
					
					if(accountExist(toUser)){
						if(!checkOnline(toUser)){
							String toUserNotOnline = toUser+" is not online!\n";
							reply(msg, toUserNotOnline);
						}
					}else{
						String toUserNotOnline = "Can't find this person: "+toUser+"\n";
						reply(msg, toUserNotOnline);
					}
					
				}
					
			}else //podcast
			{
				String msgToSend = trimmed;
				for(String user: this.onlineUser.keySet()){
					jms.convertAndSend(onlineUser.get(user), "["+fromUser+"]: "+msgToSend);
				}
			}
		}else if(msg.getJMSType().equals("show")){
			String msgToSend = getOnlineUser();
			reply(msg, "[System]: List of online users: \n"+ msgToSend);
		}
			
		}
	private String getOnlineUser() {
		// TODO Auto-generated method stub
		String result = "";
		for(String s : this.onlineUser.keySet()){
			result = "\n"+ s;
		}
		return result;
	}


	//reply to the sender of the message
	public void reply(Message msg, String msgToSend) {
		
		//convertAndSend(Destination dest, String msgToSend);
		//destionation should be the client's consumer's queue
		try {
			jms.convertAndSend(msg.getJMSReplyTo(), msgToSend);
		} catch (JmsException e) {
			System.err.println("Error: Failed to reply");
			e.printStackTrace();
		} catch (JMSException e) {
			System.err.println("Error: Failed to reply");
			e.printStackTrace();
		}
		
	}
	

	
	/*
	 * Example use: "[Allen]: hi!"
	 * will be trimed to "hi!"
	 */
	public String trimOffUserName(String msg){
		String s = msg.substring(1);
		s = s.substring(s.indexOf(']')+3, s.length());
		return s;
	}
	
	public static void setJmsTemplate(JmsTemplate jmsTemplate){
		jms = jmsTemplate;
	}
	/*
	 * This method will read to the database text file that includes all the user
	 * account name and password. Load it to the hashMap for the server.
	 */
	public void loadAccountsUser() throws IOException{
		FileReader fRead = null;
		try{
			fRead = new FileReader("Accounts.txt");
		}
		catch(FileNotFoundException e){
			System.out.println("Accounts.txt not found, writing a new file");
			PrintWriter writer = new PrintWriter("Accounts.txt", "UTF-8");
			fRead = new FileReader("Accounts.txt");
		}
		BufferedReader bReader = new BufferedReader(fRead);
		String line = null;
		while((line= bReader.readLine()) != null){
			String[] parts = line.split(" ");
			String userName = parts[0];
			String password = parts[1];
			//System.out.println(userName + " " + password);
			accounts.put(userName, password);
		}
		bReader.close();
		fRead.close();

	}
	/*
	 * This method writes the new login account info to the database text file
	 * 
	 */
	public void writeAccount(String userName, String password) throws IOException{
		FileWriter fw = new FileWriter("Accounts.txt", true);
		fw.write(userName + " " + password + "\n");
		fw.flush();
		fw.close();
	}
	

}
